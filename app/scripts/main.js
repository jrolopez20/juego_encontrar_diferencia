(function () {
    'use strict';

    var myApp = angular.module('myApp', ['timer']);

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    myApp.controller('MainController', ['$scope', function ($scope) {
        $scope.maxLevel = 3;
        $scope.pauseGame = true;
        $scope.startedGame = false;
        $scope.showNextLevelMsg = false;
        $scope.finishedGame = false;

        $scope.finalTime = '00:00:00';
        $scope.currentLevel = 1;
        $scope.points = null;
        $scope.audioClass = 'glyphicon glyphicon-volume-off';
        $scope.sound = new Audio('music/sound'+getRandomInt(1,4)+'.mp3');
        $scope.sound.loop = true;
        $scope.showCursorCopy = false;

        $scope.startGame = function () {
            trackEvent('btn_comenzar_juego','btn_comenzar_juego','btn_comenzar_juego');
            $scope.currentLevel = 1;
            $scope.pauseGame = false;
            $scope.startedGame = true;
            $scope.finishedGame = false;
            $scope.points = new Array();
            $scope.sound.play();

            $('#signals').remove();

            $scope.startTimer();
        };

        $scope.startTimer = function () {
            $scope.$broadcast('timer-start');
        };

        $scope.stopTimer = function () {
            $scope.$broadcast('timer-stop');
        };

        $scope.resumeTimer = function () {
            $scope.$broadcast('timer-resume');
        };

        $scope.resetTimer = function () {
            $scope.$broadcast('timer-reset');
        };

        $scope.savePoint = function (pointId) {
            for (var i = 0; i < $scope.points.length; i++) {
                if (pointId == $scope.points[i]) {
                    //Si ya dio click en el elemento no continua con el proceso
                    return;
                }
            }
            if($scope.points.length == 0) {
                $('#level' + $scope.currentLevel + ' div.border-left-dashed')
                    .append('<div id="signals"></div>');
            }
            //Agrega el indicador al punto seleccionado
            $scope.points.push(pointId);

            if ($scope.currentLevel == 1) {
                if (pointId == 1) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;left: 55px;top: 84px;"/>');
                }
                if (pointId == 2) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;bottom: 15px;right: 22px;"/>');
                }
                if (pointId == 3) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;left: 182px;top: 143px;"/>');
                }
                if (pointId == 4) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;bottom: 45px;right: 52px;"/>');
                }
                if (pointId == 5) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;top: 65px;right: 62px;"/>');
                }
            }
            else if ($scope.currentLevel == 2) {
                if (pointId == 1) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;left: 110px;bottom: 80px;"/>');
                }
                if (pointId == 2) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;bottom: 5px;right: 175px;"/>');
                }
                if (pointId == 3) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;left: 139px;top: 143px;"/>');
                }
                if (pointId == 4) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;top: 40px;right: 52px;"/>');
                }
                if (pointId == 5) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;bottom: 80px;right: 162px;"/>');
                }
            }
            else if ($scope.currentLevel == 3) {
                if (pointId == 1) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;left: 230px;top: 140px;"/>');
                }
                if (pointId == 2) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;bottom: 39px;left: 2px;"/>');
                }
                if (pointId == 3) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;left: 160px;bottom: 108px;"/>');
                }
                if (pointId == 4) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;top: 14px;left: 2px;"/>');
                }
                if (pointId == 5) {
                    $('#signals')
                        .append('<img src="images/signal' + $scope.points.length +
                        '.png" style="position: absolute;bottom: 170px;right: 30px;"/>');
                }
            }

            if ($scope.points.length == 5) {
                if ($scope.currentLevel < $scope.maxLevel) {
                    $scope.pauseGame = true;
                    $scope.stopTimer();
                    $scope.showNextLevelMsg = true;
                } else {
                    //Caso en que halla ganado todos los niveles
                    $scope.pauseGame = true;
                    $scope.finishedGame = true;
                    $scope.stopTimer();
                    $scope.finalTime = $('#show-time').text();
                }
            }
        };

        //Muestra mensaje de avanzar al proximo nivel
        $scope.nextLevel = function () {
            $('#signals').remove();
            $scope.pauseGame = false;
            $scope.showNextLevelMsg = false;
            $scope.currentLevel++;
            $scope.points = new Array();
            $scope.resumeTimer();
        };

        $scope.toggleAudio = function() {
            if ($scope.sound.paused) {
                $scope.audioClass = 'glyphicon glyphicon-volume-off';
                $scope.sound.play();
            }else {
                $scope.audioClass = 'glyphicon glyphicon-volume-up';
                $scope.sound.pause();
            }
        };

        $scope.onMouseEnter = function(parentDiv) {
            $scope.showCursorCopy = true;
        };

        $scope.onMouseleave = function(parentDiv) {
            $scope.showCursorCopy = false;
        };

        $( ".border-right-dashed" ).mousemove(function( event ) {
            $("#cursorCopy").css({
                "top": event.offsetY,
                "left": event.offsetX + 485
            });
        });

    }]);
})();